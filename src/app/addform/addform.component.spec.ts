import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { AddformComponent } from './addform.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient,  } from '@angular/common/http';

describe('AddformComponent', () => {
  let component: AddformComponent;
  let fixture: ComponentFixture<AddformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddformComponent ],
      imports: [ ReactiveFormsModule, HttpClientTestingModule ],
      providers: [ HttpClientTestingModule, HttpClient]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
