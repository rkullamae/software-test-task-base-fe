import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-addform',
  templateUrl: './addform.component.html',
  styleUrls: ['./addform.component.scss']
})
export class AddformComponent implements OnInit {

  messageForm: FormGroup;
  submitted = false;
  success = false;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
    this.messageForm = this.formBuilder.group( {
      firstname: ['', Validators.required],
      surname: ['', Validators.required],
      phonenumber: [
        '+4407',
        [
          Validators.required,
          Validators.pattern('^\\+4407([0-9]{9,14})')
        ]
      ]
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;

    if (this.messageForm.invalid) {
      return;
    }

    this.success = true;
  }

  createOne() {
    this.onSubmit();

    return this.http.post('http://localhost:3000/api/v1/clients', {
      'firstname': this.messageForm.value.firstname,
      'surname': this.messageForm.value.surname,
      'phoneNumber': this.messageForm.value.phonenumber
    }).subscribe((res: any) => {
      console.log(res);
    },
    (err) => {
      console.log(err.statusText);
    });
  }

}
