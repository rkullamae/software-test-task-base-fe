import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddformComponent } from './addform/addform.component';
import { AllclientsComponent } from './allclients/allclients.component';
import { NavComponent } from './nav/nav.component';
import { DeleteclientComponent } from './deleteclient/deleteclient.component';
import { HomeComponent } from './home/home.component';
import { UpdateclientComponent } from './updateclient/updateclient.component';

@NgModule({
  declarations: [
    AppComponent,
    AddformComponent,
    AllclientsComponent,
    NavComponent,
    DeleteclientComponent,
    HomeComponent,
    UpdateclientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
