import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-allclients',
  templateUrl: './allclients.component.html',
  styleUrls: ['./allclients.component.scss']
})
export class AllclientsComponent implements OnInit {

  list: object;

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  getList() {
    this.http.get('http://localhost:3000/api/v1/clients/').subscribe((response: any) => {
      this.list = response;
    });
  }
  showList() {
    this.getList();
  }

}
