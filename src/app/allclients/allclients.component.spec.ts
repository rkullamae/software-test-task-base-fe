import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllclientsComponent } from './allclients.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient,  } from '@angular/common/http';

describe('AllclientsComponent', () => {
  let component: AllclientsComponent;
  let fixture: ComponentFixture<AllclientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllclientsComponent ],
      imports: [ ReactiveFormsModule, HttpClientTestingModule ],
      providers: [ HttpClientTestingModule, HttpClient]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllclientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
