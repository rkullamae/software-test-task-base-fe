import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-deleteclient',
  templateUrl: './deleteclient.component.html',
  styleUrls: ['./deleteclient.component.scss']
})

export class DeleteclientComponent implements OnInit {

  messageForm: FormGroup;

  submitted = false;
  success = false;
  errorMessage = '';

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
    this.messageForm = this.formBuilder.group({
      id: ['', Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.messageForm.invalid) {
      return;
    }

    this.success = true;
  }

  ngOnInit() {
  }

  deleteOne(e) {
    this.onSubmit();

    return this.http.delete('http://localhost:3000/api/v1/clients/' + this.messageForm.controls['id'].value)
    .subscribe((res: any) => {
      this.errorMessage = '';
    },
    (err) => {
      this.errorMessage = `Error message: ${err.statusText}`;
    }
    );
  }

}
