import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteclientComponent } from './deleteclient.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient,  } from '@angular/common/http';

describe('DeleteclientComponent', () => {
  let component: DeleteclientComponent;
  let fixture: ComponentFixture<DeleteclientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteclientComponent ],
      imports: [ ReactiveFormsModule, HttpClientTestingModule ],
      providers: [ HttpClientTestingModule, HttpClient]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
