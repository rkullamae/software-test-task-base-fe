import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-updateclient',
  templateUrl: './updateclient.component.html',
  styleUrls: ['./updateclient.component.scss']
})

export class UpdateclientComponent implements OnInit {

  messageForm: FormGroup;

  submitted = false;
  success = false;
  errorMessage = '';

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
    this.messageForm = this.formBuilder.group({
      id: ['', Validators.required],
      firstname: ['', Validators.required],
      surname: ['', Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.messageForm.invalid) {
      return;
    }

    this.success = true;
  }

  ngOnInit() {
  }

  updateOne(e) {
    this.onSubmit();

    return this.http.put('http://localhost:3000/api/v1/clients/' + this.messageForm.controls['id'].value, {
      'firstname': this.messageForm.controls.firstname.value,
      'surname': this.messageForm.controls.surname.value,
    }).subscribe((res: any) => {
      this.errorMessage = '';
    },
    (err) => {
      this.errorMessage = `Error message: ${err.statusText}`;
    }
    );
  }

}
