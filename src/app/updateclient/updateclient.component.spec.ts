import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateclientComponent } from './updateclient.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient,  } from '@angular/common/http';

describe('UpdateclientComponent', () => {
  let component: UpdateclientComponent;
  let fixture: ComponentFixture<UpdateclientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateclientComponent ],
      imports: [ ReactiveFormsModule, HttpClientTestingModule ],
      providers: [ HttpClientTestingModule, HttpClient]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
